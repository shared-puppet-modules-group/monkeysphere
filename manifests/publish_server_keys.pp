# Server host key publication
define monkeysphere::publish_server_keys ( $keyid = '--all' ) { 
  exec { "monkeysphere-host publish-keys $keyid":
    environment => "MONKEYSPHERE_PROMPT=false",
    require => [ Package["monkeysphere"], Exec["monkeysphere-import-key"], File["monkeysphere_host_conf"] ];
  }
}
