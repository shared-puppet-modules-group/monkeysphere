define monkeysphere::authorized_user_ids(
  $user_ids,
  $dest_dir = '/root/.monkeysphere',
  $dest_file = 'authorized_user_ids',
  $group = '') {

  $user = $title
  $calculated_group = $group ? {
    '' => $user,
    default => $group
  }

  # don't require user if it's root because root is not handled 
  # by puppet
  case $user {
    root: {
      file {
        $dest_dir:
          owner => $user,
          group => $calculated_group,
          mode => 755,
          ensure => directory,
      }
    }
    default: {
      file {
        $dest_dir:
          owner => $user,
          group => $calculated_group,
          mode => 755,
          ensure => directory,
          require => User[$user]
      }
    }
  }

  file {
    "${dest_dir}/${dest_file}":
      owner => $user,
      group => $calculated_group,
      mode => 644,
      content => template('monkeysphere/authorized_user_ids.erb'),
      ensure => present,
      recurse => true,
      require => File[$dest_dir] 
  }

  exec { "monkeysphere-authentication update-users $user":
    refreshonly => true,
    require => [ File["monkeysphere_authentication_conf"], Package["monkeysphere"] ],
    subscribe => File["${dest_dir}/${dest_file}"] 
  }
}
