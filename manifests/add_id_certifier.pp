# add certifiers
define monkeysphere::add_id_certifier( $keyid ) {
  exec { "monkeysphere-authentication add-id-certifier $keyid":
	  environment => "MONKEYSPHERE_PROMPT=false",
	  require => [ Package["monkeysphere"], File["monkeysphere_authentication_conf"] ],
	  unless => "/usr/sbin/monkeysphere-authentication list-id-certifiers | grep $keyid > /dev/null"
  }
}
