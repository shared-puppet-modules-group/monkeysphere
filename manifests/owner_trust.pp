define monkeysphere::owner_trust (
  $fingerprint,
  $user = 'root',
  $level = 6 ) {

  $keyserver_arg = $monkeysphere_keyserver ? {
    '' => '',
    default => "--keyserver $monkeysphere_keyserver"
  }

  # ensure the key is in the key ring
  exec { "monkeysphere-gpg-recv-key-$user-$fingerprint":
    command => "gpg $keyserver_arg --recv-key $fingerprint",
    require => [ Package["monkeysphere"] ],
    user => $user,
    unless => "gpg --list-key $fingerprint 2>&1 >/dev/null"
  }
  # provide ownertrust
  exec { "monkeysphere-gpg-ownertrust-$user-$fingerprint":
    command => "printf '$fingerprint:$level\n'\$(gpg --export-ownertrust) | gpg --import-ownertrust",
    require => [ Package["monkeysphere"] ],
    user => $user,
    unless => "gpg --export-ownertrust | grep $fingerprint >/dev/null"
  }
}
