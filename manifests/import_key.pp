define monkeysphere::import_key (
  $scheme = 'ssh://',
  $port = '',
  $path = '/etc/ssh/ssh_host_rsa_key',
  $hostname = $fqdn ) {

  # if we're getting a port number, prefix with a colon so it's valid
  $prefixed_port = $port ? {
    '' => '',
    default => ":$port"
  }

  $key = "${scheme}${fqdn}${prefixed_port}"

  exec { "monkeysphere-host import-key $path $key":
    alias => "monkeysphere-import-key",
    require => [ Package["monkeysphere"],  File["monkeysphere_host_conf"] ],
    unless => "/usr/sbin/monkeysphere-host s | grep $key > /dev/null"
  }
}
