# include to export your ssh key
class monkeysphere::sshserver {
  include monkeysphere
  if $::monkeysphere_has_hostkey {
    @@file { "/var/lib/puppet/modules/monkeysphere/hosts/${::fqdn}":
      ensure  => present,
      content => template('monkeysphere/host.erb'),
      require => Package['monkeysphere'],
      tag     => 'monkeysphere-host',
    }
  }

  cron {'update-monkeysphere-auth':
    command => '/usr/sbin/monkeysphere-authentication update-users > /dev/null 2>&1',
    user    => root,
    minute  => fqdn_rand(60),
    require => Package['monkeysphere'],
  }
}
